import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import SearchResults from "../views/SearchResults.vue";
import Title from "../views/Title.vue";
import nProgress from "nprogress";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/search",
    name: "SearchResults",
    component: SearchResults,
  },
  {
    path: "/tv/:id",
    name: "TV",
    component: Title,
  },
  {
    path: "/movie/:id",
    name: "Movie",
    component: Title,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeResolve((to, from, next) => {
  if (to.name) {
    nProgress.start();
  }
  next();
});

router.afterEach((to, from) => {
  nProgress.done();
});

export default router;
