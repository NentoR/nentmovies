import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

const movie = {
  namespaced: true,
  state: {
    title: {},
    videos: {},
    images: [],
    cast: [],
    keywords: [],
  },
  getters: {
    title: (state) => state.title,
    videos: (state) => state.videos,
    images: (state) => state.images,
    cast: (state) => state.cast,
    keywords: (state) => state.keywords,
  },
  mutations: {
    setTitle(state, obj) {
      state.title = obj.data;
    },
    setVideos(state, obj) {
      state.videos = obj.data.results;
    },
    setImages(state, images) {
      state.images = images;
    },
    setCast(state, cast) {
      state.cast = cast;
    },
    setKeywords(state, keywords) {
      state.keywords = keywords;
    },
  },
  actions: {
    async fetchTitle(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const movie = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}?api_key=${rootState.apiKey}&language=en-US`
      );

      const trailer = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}/videos?api_key=${rootState.apiKey}&language=en-US`
      );
      movie.data.trailer = trailer.data.results[0];

      store.commit("setTitle", movie);
    },
    async fetchVideos(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const videos = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}/videos?api_key=${rootState.apiKey}&language=en-US`
      );

      store.commit("setVideos", videos);
    },
    async fetchImages(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const images = [];

      const imagesObj = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}/images?api_key=${rootState.apiKey}`
      );

      imagesObj.data.backdrops.forEach((backdrop) => images.push(backdrop));

      store.commit("setImages", images);
    },
    async fetchCast(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const castObj = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}/credits?api_key=${rootState.apiKey}&language=en-US`
      );

      store.commit("setCast", castObj.data.cast);
    },
    async fetchKeywords(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const keywordsObj = await axios.get(
        `${rootState.apiBaseURL}/movie/${id}/keywords?api_key=${rootState.apiKey}`
      );

      store.commit("setKeywords", keywordsObj.data.results);
    },
  },
};

const tv = {
  namespaced: true,
  state: {
    title: {},
    videos: {},
    images: [],
    cast: [],
    keywords: [],
  },
  getters: {
    title: (state) => state.title,
    videos: (state) => state.videos,
    images: (state) => state.images,
    cast: (state) => state.cast,
    keywords: (state) => state.keywords,
  },
  mutations: {
    setTitle(state, obj) {
      state.title = obj.data;
    },
    setVideos(state, obj) {
      state.videos = obj.data.results;
    },
    setImages(state, images) {
      state.images = images;
    },
    setCast(state, cast) {
      state.cast = cast;
    },
    setKeywords(state, keywords) {
      state.keywords = keywords;
    },
  },
  actions: {
    async fetchTitle(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const tv = await axios.get(
        `${rootState.apiBaseURL}/tv/${id}?api_key=${rootState.apiKey}&language=en-US`
      );

      const trailer = await axios.get(
        `${rootState.apiBaseURL}/tv/${id}/videos?api_key=${rootState.apiKey}&language=en-US`
      );
      tv.data.trailer = trailer.data.results[0];

      store.commit("setTitle", tv);
    },
    async fetchVideos(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const videos = await axios.get(
        `${rootState.apiBaseURL}/tv/${id}/videos?api_key=${rootState.apiKey}&language=en-US`
      );

      store.commit("setVideos", videos);
    },
    async fetchImages(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      let imagesObj = null;
      const images = [];

      for (let i = 1; i <= 5; i++) {
        imagesObj = await axios.get(
          `${rootState.apiBaseURL}/tv/${id}/season/1/episode/${i}/images?api_key=${rootState.apiKey}`
        );

        imagesObj.data.stills.forEach((still) => images.push(still));
      }

      store.commit("setImages", images);
    },
    async fetchCast(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const castObj = await axios.get(
        `${rootState.apiBaseURL}/tv/${id}/credits?api_key=${rootState.apiKey}&language=en-US`
      );

      store.commit("setCast", castObj.data.cast);
    },
    async fetchKeywords(store, payload) {
      const { rootState } = store;
      const { id } = payload;

      const keywordsObj = await axios.get(
        `${rootState.apiBaseURL}/tv/${id}/keywords?api_key=${rootState.apiKey}`
      );

      store.commit("setKeywords", keywordsObj.data.results);
    },
  },
};

export default new Vuex.Store({
  state: {
    searchResults: [],
    apiKey: "0c7699be956443f824378e7b713848d8",
    apiBaseURL: "https://api.themoviedb.org/3",
  },
  mutations: {
    setSearchResults(state, array) {
      state.searchResults = array;
    },
  },
  getters: {
    searchResults: (state) => state.searchResults,
  },
  actions: {
    async fetchSearchResults(store, { type, query }) {
      const apiResults = await axios.get(
        `${store.state.apiBaseURL}/search/multi?api_key=${store.state.apiKey}&language=en-US&query=${query}&include_adult=false`
      );

      const searchResults = [];

      apiResults.data.results.forEach((result) => {
        if (result.media_type === "movie" || result.media_type === "tv") {
          searchResults.push(result);
        }
      });

      store.commit("setSearchResults", searchResults);
    },
  },
  modules: {
    tv,
    movie,
  },
});
