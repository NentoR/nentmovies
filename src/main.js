import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Buefy from "buefy";
import "./assets/scss/app.scss";
import "bulmaswatch/darkly/bulmaswatch.scss";
import axios from "axios";
import nProgress from "nprogress";

Vue.prototype.$http = axios;

axios.interceptors.request.use((config) => {
  nProgress.start();
  return config;
});

axios.interceptors.response.use((response) => {
  nProgress.done();
  return response;
});

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
